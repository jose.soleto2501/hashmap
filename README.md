## HashMap – Alumnos
- [ ] Crea una clase Colegio que almacene el pueblo de origen de los alumnos de un colegio.
La clase tendrá los siguientes métodos:
## addAlumno(String pueblo)
- [ ] añade el pueblo de un nuevo alumno
## showAll()
- [ ] Muestra los distintos pueblos y el número de alumnos que hay por cada pueblo.
## showPueblo(String pueblo)
- [ ] Muestra el pueblo y el número de alumnos de ese pueblo
## cuantos()
- [ ] Muestra cuántos pueblos diferentes existen en el colegio.
#borra()
- [ ] Elimina los datos insertados.
##IMPORTANTE
Crea también una clase para testear la clase anterior donde almacenes al menos 20 alumnos
de distintos pueblos repitiendo algunos.--

## Nota: Los pueblos se almacenan en mayúsculas.