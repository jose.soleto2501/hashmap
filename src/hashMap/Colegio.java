package hashMap;


import java.util.HashMap;
import java.util.Map;

public class Colegio {
    private HashMap<String,Integer> alumnos;


    public Colegio() {
        alumnos = new HashMap<>();

    }

    public void addAlumno(String pueblo) {
        pueblo = pueblo.toUpperCase();
        alumnos.put(pueblo, alumnos.getOrDefault(pueblo,0)+1);
    }

    public void showAll() {
        //Comprobamos con el método isEmpty que devuelve verdadero si este mapa no contiene asignaciones clave-valor.
        if (alumnos.isEmpty()){
            System.out.println("No hay alumnos");
        } else {
            //Si hay algo recorremos el hashmap azi (no se ni como funcionaxd)
            for (Map.Entry<String,Integer> alumnos : alumnos.entrySet()) {
                System.out.println("Pueblo: " + alumnos.getKey() + ", Alumnos: " + alumnos.getValue());
            }
        }

    }

    public void showPueblo(String pueblo) {
        //Pasamos pueblo a mayúsculas
        pueblo = pueblo.toUpperCase();
        //Hacemos un if para comprobar si pueblo tiene alguna llave, el método containsKey devuelve TRUE o false
        if (alumnos.containsKey(pueblo)) {
            //Si es cierto mostraremos el pueblo que hemos recogido antes y que ya hemos pasado a mayus y la llave
            System.out.println("Pueblo: " + pueblo + ", Alumnos: " + alumnos.get(pueblo));
        } else {
            //Si no pues decimos que noxd
            System.out.println("No hay alumnos en" + pueblo);
        }
    }

    public void cuantos() {
        //Mostramos un mensaje con el tamaño de los pueblos :)
        System.out.println("Pueblos diferentes en el colegio: " + alumnos.size());
    }

    public void borrar() {
        //El método clear elimina todas las asignaciones de este mapa
        alumnos.clear();
    }

}
