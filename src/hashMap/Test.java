package hashMap;



public class Test {
    public static void main(String[] args) {
        Colegio colegio = new Colegio();
        colegio.addAlumno("Mollet");
        colegio.addAlumno("Rubi");
        colegio.addAlumno("Hospitalet");
        colegio.addAlumno("Calafell");
        colegio.addAlumno("Hospitalet");
        colegio.addAlumno("Calafell");
        colegio.addAlumno("Rubi");
        colegio.addAlumno("Rubi");
        colegio.addAlumno("Hospitalet");
        colegio.addAlumno("Calafell");
        colegio.addAlumno("Mollet");
        colegio.addAlumno("Rubi");
        colegio.addAlumno("Hospitalet");
        colegio.addAlumno("Calafell");
        colegio.addAlumno("Hospitalet");
        colegio.addAlumno("Calafell");
        colegio.addAlumno("Rubi");
        colegio.addAlumno("Rubi");
        colegio.addAlumno("Hospitalet");
        colegio.addAlumno("Calafell");
        //Mostramos
        System.out.println("Todos los pueblos y número de alumnos:");
        colegio.showAll();

        //Muestra el numero de pueblos que hay
        colegio.cuantos();

        // Muestra el pueblo y el num de alumnos
        System.out.println("\nAlumnos en Mollet:");
        colegio.showPueblo("Mollet");
        System.out.println("\nAlumnos en Rubi");
        colegio.showPueblo("Rubi");

        // Elimina los datos insertados
        colegio.borrar();
        System.out.println("\nDespués de borrar a los alumnos:");
        colegio.showAll();
    }
}
